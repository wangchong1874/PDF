package com.uwjx.pdfexample;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleBuildPdf {

    private final static Integer CELL_ROW = 6;

    private final static String FILE_PATH = "C:\\Users\\uwjx-pc-101\\Desktop\\example.pdf";

    private final static String IMG_URL = "C:\\Users\\uwjx-pc-101\\Desktop\\1.jpg";


    public static void main(String[] args) throws Exception {
        File file = new File(FILE_PATH);
        file.getParentFile().mkdirs();
        new SimpleBuildPdf().manipulatePdf(FILE_PATH);
    }


    protected void manipulatePdf(String dest) throws Exception {
        List<String> collect = new ArrayList<>();
        collect.add("序号");
        collect.add("江苏省代码");
        collect.add("职工姓名");
        collect.add("身份证号码");
        collect.add("月均收入");
        collect.add("职工签名");
        Document document = new Document();


        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();







        BaseFont bfChinese = BaseFont.createFont("STSongStd-Light",
                "UniGB-UCS2-H", false);




        Font  font = new Font(bfChinese, 8 ,Font.NORMAL);

        Font  titleFont = new Font(bfChinese, 12 ,Font.NORMAL);
        Paragraph paragraph = new Paragraph("XXX_ 单位 XXX 年度职工收入签认表",titleFont);
        paragraph.setSpacingAfter(15);

        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);

        Paragraph department  = new Paragraph("部门"+ "",titleFont);
        department.setAlignment(Element.ALIGN_LEFT);
        department.setIndentationLeft(55);
        department.setSpacingAfter(15);
        document.add(department);


        PdfPTable table = new PdfPTable(6);

        /*表头*/
            for(int aw = 0; aw < 6; aw++){
                Paragraph a = new Paragraph(collect.get(aw), font);
//                Paragraph a = new Paragraph("411302199712244255", font);
                a.setPaddingTop(50);
                table.addCell(a);
            }



        for (int index = 0 ; index< 4; index ++){
            for(int aw = 0; aw < 6; aw++){
                /*打印 内容*/
                if (aw == collect.size() -1){
                    PdfPCell imageCell = createImageCell(IMG_URL);
                    imageCell.setPaddingTop(50);
                    table.addCell(imageCell);
                    break;
                }
            }
        }
//        table.addCell("江苏省代码");
//        table.addCell("职工姓名");
//        table.addCell("身份证号码");
//        table.addCell("月均收入");
//        table.addCell("职工签名");
        document.add(table);

        document.close();

    }

    public static PdfPCell createImageCell(String path) throws DocumentException, IOException {
        Image img = Image.getInstance(path);
        PdfPCell cell = new PdfPCell(img, true);
        return cell;
    }


    private static Font getChineseFont() {
        BaseFont base = null;
        Font fontChinese = null;
        try {
            base = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",
                    BaseFont.EMBEDDED);
            fontChinese = new Font(base, 12, Font.NORMAL);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fontChinese;
    }
}
